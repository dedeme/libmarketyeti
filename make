#!/bin/bash

PRG=libmarketyeti
LIB=/dm/dmYeti/lib/libdmyeti/pack/libdmyeti.jar

# ---------------------------------------------------------
YETI=/dm/dmYeti/tools/yeti.jar
case $1 in
project*)
  mkdir src
  mkdir classes
  mkdir pack
  mkdir tests
  mkdir tests/src
  mkdir tests/classes
  cd tests/src
  echo 'sys = load es.dm.sys;' > all.yeti
  echo '' >> all.yeti
  echo 'sys.init "libdmyetiTests";' >> all.yeti
  echo 'println "here";' >> all.yeti
  cd ../..
  ;;
c*)
  java -jar $YETI -cp $LIB -d classes src/*.yeti
  java -jar $YETI -cp classes:$LIB -d tests/classes tests/src/all.yeti
  if [ -e tests/resources ]
  then
    rm -fR test/resources
    cp -fR tests/resources tests/classes
  fi
  ;;
x*)
  java -jar $YETI -cp $LIB -d classes src/*.yeti
  java -jar $YETI -cp classes:$LIB -d tests/classes tests/src/all.yeti
  if [ -e tests/resources ]
  then
    rm -fR test/resources
    cp -fR tests/resources tests/classes
  fi
  java -cp $YETI:$LIB:classes:tests/classes all
  ;;
pack*)
  java -jar $YETI -cp $LIB -d classes src/*.yeti
  if [ -e pack/tmp ]
  then
    rm -fR pack/tmp
  fi
  mkdir pack/tmp
  cd pack/tmp
#  jar xvf $YETI
  LIBS=`echo $LIB | sed s/:/' '/g`
  for L in $LIBS
  do
    jar xvf $L
  done
  cp -fR ../../classes/* ./
#  rm -fR META-INF
  mkdir META-INF
  echo 'Manifest-Version: 1.0' > META-INF/MANIFEST.MF
  echo 'Created-By: ºDeme' >> META-INF/MANIFEST.MF
#  echo 'Main-Class: Main' >> META-INF/MANIFEST.MF
  echo ''>> META-INF/MANIFEST.MF
  jar cvfm ../$PRG.jar META-INF/MANIFEST.MF *
  cd ../..
  rm -fR pack/tmp
  ;;
*)
  echo $1: Unknown option
  ;;
esac
