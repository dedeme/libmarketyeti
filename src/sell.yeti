// Copyright 20-Sep-2018 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Market a sell order
module es.dm.market.sell;

load es.dm.json;
exc = load es.dm.exc;
fees = load es.dm.market.fees;

typedef t = {
  /// (int) Id of a company
  nick is number,
  /// (int) Number of stocks to sell
  stocks is number,
  /// true: limit order, false: market order
  limited is boolean,
  /// (double) Minimum price if limit is true
  ///
  /// Its value is 0 if limited is false (sell at market price)
  price is number,
  /// `[do_ price]` makes a sell and returns its incomes substracting fees.
  /// If the operation can not be done, it returns 0.
  do_ is number -> number,
  toJson is () -> json
};

(
  _income stocks price =
    if price < 0.01 then throw (exc.error "price \(price) < 0.01")
    else (
      incom = stocks * price;
      incom - fees.app(incom)
    )
    fi;

  _do limited price stocks p =
    if limited and p < price then 0
    else _income stocks p
    fi;

  _toJson nick stocks limited price = warray (array [
      wnumber nick,
      wnumber stocks,
      wboolean limited,
      wnumber price
    ]);


  _mk nick stocks limited price =
    {
      nick, stocks, limited, price,
      do_ p = _do limited price stocks p,
      toJson = \(_toJson nick stocks limited price)
    };

{
  /// `[mk nick stocks]` creates a new sell.t 'at market price'
  mk nick stocks
  is number -> number -> t =
    _mk nick stocks false 0,

  /// `[mkLimited]` creates a new sell.t 'limited'
  mkLimited nick stocks price
  is number -> number -> number -> t =
    _mk nick stocks true price,

  fromJson js
  is json -> t = (
    ajs = rarray js;
    _mk (rnumber ajs[0])
        (rnumber ajs[1])
        (rboolean ajs[2])
        (rnumber ajs[3])
  ),

  /// `[income stocks price]` returns the incomes of a sell, substracting fees.
  income is number -> number -> number = _income,

})
